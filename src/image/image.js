import bca from './bca.png'
import bni from './bni.png'
import bri from './bri.png'
import cc from './cc.png'
import dana from './dana.png'
import gopay from './gopay.png'
import linkaja from './linkaja.png'
import mandiri from './mandiri.png'
import ovo from './ovo.png'
import permata from './permata.png'

export default function image (data) {
  switch (data) {
    case 'bca':
      return bca
    case 'bni':
    return bni
    case 'bri':
      return bri
    case 'cc':
      return cc
    case 'dana':
      return dana
    case 'gopay':
      return gopay
    // case 'linkaja':
      // return linkaja
    case 'mandiri':
      return mandiri
    case 'ovo':
      return ovo
    case 'permata':
      return permata
    default:
      break;
  }
}