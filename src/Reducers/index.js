import { combineReducers } from 'redux';

const dataHalosis = {
    usersuccessget: '',
    dataListPayment: '',
    dataVa: '',
    dataOrder: null,
    dataAfterPayEwallet: null,
    dataChargeCC: null
  }
  
  const halosisReducers = (state = dataHalosis, action) => {
    switch (action.type) {
      case 'GET_BASSIC':
        return { ...state, usersuccessget: action.value }
      case 'GET_PAYMENT_METHOD':
        return { ...state, dataListPayment: action.value }
      case 'GET_DATA_VA':
        return { ...state, dataVa: action.value }
      case 'GET_DATA_ORDER':
        return { ...state, dataOrder: action.value }
      case 'GET_PAY_EWALLET':
        return { ...state, dataAfterPayEwallet: action.value }
      case 'GET_CHARGECC_DATA':
        return { ...state, dataChargeCC: action.value }
      default:
        return state;
    }
  };
  
  export default combineReducers({
    usersuccessget: halosisReducers,
  });