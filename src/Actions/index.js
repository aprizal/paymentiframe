import axios from 'axios'
let apiKey =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5FSkRSRUUxT1RRNFEwRXdRamc0UTBJM1JqUkVRamhCT1RGRVFUaEZRVEl6TnpCQlFURXhOZyJ9.eyJpc3MiOiJodHRwczovL2Rldi1zYWhhYmF0aGFsb3Npcy5hdXRoMC5jb20vIiwic3ViIjoiaEhIdDlNMXk1ZjQyd3I3SGVKVElSczRKbDVDSTVFckpAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vc2gtYXBpIiwiaWF0IjoxNTY4NTk1MDYyLCJleHAiOjE1NzExODcwNjIsImF6cCI6ImhISHQ5TTF5NWY0MndyN0hlSlRJUnM0Smw1Q0k1RXJKIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.uzw6-SSxfs4UmcSfKoXdBUtJWnOG_IW6Z7LbENC2NfmlsOp-0YeopRjcQSFBM3tEIRQlbTdfhIZSGznYODfzIdPMlSSl62YKNGky-W1yVrvA71PtTZKvZV1GwUy_5buTk0dUGPSYEfCJTVnaV_OsZwYKkFCEkw9v28MZBtlb7l5kdu210dDRRsi4P6mb4-vSF8loCOdQ8RSvbkEX-xrEvzsc6ep93tqymA2pDbPrj29kmyDh4Xi57f5LQRdbWqbTnnkhbHVf9xsu0O83pM04c_QJpXJkFeMwVCbNAXmYVBJjNU3BJqEGF-uowbsuXEdjIVj13sqneIsJyv0oj0AXKg==halosis";


const configAPI = () => {
  // if (window.location.hostname == 'localhost') {
    // return 'http://localhost:3000/'   
  // } else {
    return 'https://api.v3.halosis.co.id/'
  // }
}

const configRedirect = () => {
  // if (window.location.hostname == 'localhost') {
    // return 'https://stg.pay.halosis.cloud/'   
  // } else {
    return 'https://pay.halosis.co.id/thankyou'
  // }
}

import {generateDateTimeForExternalID} from '../Helper/index'

const getBassic = (value) => {
  return {
  type: 'GET_BASSIC',
    value
  }
};
const getTagDataListPayment = (value) => {
  return {
  type: 'GET_PAYMENT_METHOD',
    value
  }
};
const getTagDataVaPayment = (value) => {
  return {
  type: 'GET_DATA_VA',
    value
  }
};
const getTagDataOrder = (value) => {
  return {
  type: 'GET_DATA_ORDER',
    value
  }
};
const getTagPayOrderEwallet = (value) => {
  return {
  type: 'GET_PAY_EWALLET',
    value
  }
};
const getTagchargeCC = (value) => {
  return {
    type: 'GET_CHARGECC_DATA',
      value
    }
}
// ============================================================>
export const bassic = (value) => {  
  return (dispatch) => {
    let data = 'test'
    return dispatch(getBassic(data))
  };
};

export const getDataListPayment = value => {
  return dispatch => {
    axios
      .get(`${configAPI()}api/vendors/payments/${String(value)}`, {
        headers: {
          Authorization: `Bearer ${apiKey}`,
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      })
      .then(function(response) {
        return dispatch(getTagDataListPayment(response.data));
      })
      .catch(function(error) {});
  };
};

export const getDataVaPayment = (value, id) => {
  return dispatch => {
    let order_number = "0"
    axios(`${configAPI()}v3/orders/order_number/${id}`, {
      method: "get",
      headers: {
        "Authorization": `Bearer ${apiKey}`,
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    }).then(r => {
    order_number = r.data.payload.data.order_number  
    axios(`${configAPI()}api/virtualAccount/createVirtualAccount`, {
        method: "post",
        headers: {
          "Authorization": `Bearer ${apiKey}`,
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        data: {
          external_id: order_number,
          bank_code: value.channel_code.toUpperCase(),
          name: value.vendor_name
        }
      })
      .then((response) => {
        console.log('va => ', response)
        return dispatch(getTagDataVaPayment(response));
      })
      .catch(function(error) {
        console.log(error)
      });
    }).catch(e => console.log('error order num => ', e))

  };
};

export const getDataOrder = value => {
  return dispatch => {
    axios
      .get(
        `${configAPI()}v3/orders/detail/payment/${String(value)}`,
        {
          headers: {
            Authorization: `Bearer ${apiKey}`,
            "Content-Type": "application/json",
            Accept: "application/json"
          }
        }
      )
      .then(function(response) {
        if (response.data.payload.total > 0) {
          console.log('resp ==>> ', response.data)
          return dispatch(getTagDataOrder(response.data.payload));
        }
        // alert(response.data.message)
      })
      .catch(function(error) {
        console.log(error)
      });
  };
};

export const getPayOrderEwallet = (value, valuePayment, dataProduk) => {
  const total_bayar = parseInt(value.total_price) + parseInt(value.delivery_price)
  let parameters = {
    external_id: value.order_number,
    amount: total_bayar,
    ewallet_type: valuePayment.channel_code,
    expiration_date: "2020-12-27T08:42:54.308Z",
    callback_url: "https://us-central1-halosis-yfsjsn.cloudfunctions.net/production_onPaymentSuccess",
    redirect_url: configRedirect(),
    phone: value.delivery_phone,
    items: []
  };

  console.log('param ewallet => ', parameters)

  for (let index = 0; index < dataProduk.order_items.length; index++) {
    let object = {
      id: String(dataProduk.order_items[index].product_id),
      price: dataProduk.order_items[index].price
      // quantity: dataProduk.order_items[index].item_quantity
        // ? dataProduk.order_items[index].item_quantity
        // : 0
    };
    parameters.items.push(object);
  }

  return dispatch => {
    axios({
      url: `${configAPI()}api/ewallet`,
      method: "post",
      headers: {
        Authorization: `Bearer ${apiKey}`,
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      data: parameters
    })
      .then(function(response) {
        console.log('api ewallet => ', JSON.stringify(response.data))
        return dispatch(getTagPayOrderEwallet(response));
      })
      .catch(function(error) {
        console.log('err => ', error) 
        // alert(JSON.stringify(error));
      });
  };
};

export const chargeCC = (value, amount, id) => {
  let parameters = {
    externalID: id,
    tokenID: value.id,
    amount: Number(amount),
    authID: value.authentication_id
  };

  return dispatch => {
    axios
      .post(`${configAPI()}api/card/charge`, {
        headers: {
          Authorization: `Bearer ${apiKey}`,
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        data: parameters
      })
      .then(function(response) {
        return dispatch(getTagchargeCC(response));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
};
