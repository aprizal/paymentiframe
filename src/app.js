import React, { Component } from 'react';
import { connect } from 'react-redux'
import store from './Store/index'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { bassic } from './Actions/index'
import './App.css';
import { addAllPrice } from './Helper/index'
import Loading from './MainComponent/loading'
import ThankYou from './MainComponent/ThankYouPage/newThanksPage'
import ThankYouVms from './MainComponent/ThankYouPage/ThanksPageVms'

import ChoosePaymentMethod from './MainComponent/choosePaymentMethod'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      route: null
    }
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={Loading} />
            <Route exact path="/thankyou" component={ThankYou} />
            <Route exact path="/thankyouvms" component={ThankYouVms} />
            <Route exact path="/:id" component={() => <ChoosePaymentMethod route={this.state.route} />} />
          </Switch>
        </Router>
      </div>
    )
  }

  componentDidMount() {
    let cutLink = window.location.pathname.split('/')
    var filtered = cutLink.filter(function (el) {
      return el != null;
    });

    // for (let index = 0; index < cutLink.length; index++) {
    //   console.log('pp'+cutLink[index])
    // }

    this.setState({ route: filtered.join('') }, () => {
      // console.log('xx the route (hapus) => ', Number(this.state.route))
    })
    this.props.bassic()
  }

  componentWillReceiveProps(nextProps) {

  }
}

const mapState = state => {
  return {
    // usersuccessget: state.usersuccessget.usersuccessget
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    bassic: () => dispatch(bassic()),
  }
}

export default connect(mapState, mapDispatchToProps)(App)