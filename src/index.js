// import React from 'react';
//   import { render } from 'react-dom';

//   const rootElement = document.getElementById('react-app');

//   render(<div> Hello World! </div>, rootElement);


import React from 'react';
import { render } from 'react-dom';
import store from './Store/index'
import { Provider } from 'react-redux'
import { history } from './Store'
import App from './app';
import './index.css';
import {BrowserRouter} from 'react-router-dom';

const rootEl = document.getElementById('app');
render(<Provider store={store}><BrowserRouter><App /></BrowserRouter></Provider>, rootEl);

if (module.hot) {
    module.hot.accept();
}