import moment from "moment"

export const addAllPrice = (data) => {
    let cutLink = data.splice('/')
    return cutLink
}

export const generateDateTimeForExternalID = (data) => {
    let date = new Date()
    let dateDay = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()
    let hours = date.getHours()
    let minute = date.getMinutes()
    let seconds = date.getSeconds()

    let generate = `${dateDay}${month}${year}${hours}${minute}${seconds}`
    return generate
}

export const datePlus1 = (data) => {
  let tomorrow = new Date();
  tomorrow = moment(data).add(1, 'day').format();
  // console.log('tommorow: ', tomorrow)
  return tomorrow
}

export const convertToRupiah = (data) => {
    let angka = data ? parseInt(data) : 0;
    var rupiah = "";
    var angkarev = angka
      .toString()
      .split("")
      .reverse()
      .join("");
    for (var i = 0; i < angkarev.length; i++)
      if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
    return (
      "Rp. " +
      rupiah
        .split("", rupiah.length - 1)
        .reverse()
        .join("")
    );
  }