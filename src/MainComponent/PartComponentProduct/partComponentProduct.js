import React, { Component } from "react";
import Media from "react-media";
import {convertToRupiah} from "../../Helper"
class PartComponentProduct extends Component {
  render() {
    console.log('prop => ', this.props.data)
    return (
      <div
        className="row"
        style={{ paddingTop: "10px", marginLeft: "0", marginRight: "0" }}
      >
        <Media queries={{ small: { maxWidth: 410 } }}>
          {matches =>
            matches.small ? (
              <div
                className="col-sm-3"
                style={{ padding: "0", margin: "0", textAlign: "center" }}
              >
                {!this.props.data.product_image ? 
                <img
                src={`https://storage.googleapis.com/halosis-v3/${this.props.data.product.product_images[0].filename}`}
                alt="Halosis"
                style={{ width: "50%", height: "auto" }}
              /> : <img
              src={`https://storage.googleapis.com/halosis-v3/${this.props.data.product_image}`}
              alt="Halosis"
              style={{ width: "50%", height: "auto" }}
            />}
              </div>
            ) : (
              <div
                className="col-sm-3"
                style={{ padding: "0", margin: "0", alignSelf: "center" }}
              >
                  {!this.props.data.product_image ? 
                <img
                src={`https://storage.googleapis.com/halosis-v3/${this.props.data.product.product_images[0].filename}`}
                alt="Halosis"
                style={{ width: "50%", height: "auto" }}
              /> : <img
              src={`https://storage.googleapis.com/halosis-v3/${this.props.data.product_image}`}
              alt="Halosis"
              style={{ width: "50%", height: "auto" }}
            />}
              </div>
            )
          }
        </Media>

        <Media queries={{ small: { maxWidth: 410 } }}>
          {matches =>
            matches.small ? (
              <div
                className="col-sm-9"
                style={{ paddingRight: "15px", margin: "0" }}
              >
                <p
                  style={{
                    margin: "0",
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold"
                  }}
                >
                  {this.props.data.product_name}
                </p>
                <p
                  style={{
                    margin: "0",
                    fontSize: "10px",
                    paddingTop: "0px",
                    fontWeight: "bold"
                  }}
                >
                  {this.props.data.description}
                </p>
                <p
                  style={{
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold",
                    color: "rgb(88, 34, 193)"
                  }}
                >
                  {convertToRupiah(this.calculatePrice(this.props.data))}
                </p>
                <p
                  style={{
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold",
                    color: "grey"
                  }}
                >
                  {this.functForDataQwt(this.props.data)}
                </p>
              </div>
            ) : (
              <div
                className="col-sm-9"
                style={{ paddingRight: "0", margin: "0" }}
              >
                <p
                  style={{
                    margin: "0",
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold"
                  }}
                >
                  {this.props.data.product_name}
                </p>
                <p
                  style={{
                    margin: "0",
                    fontSize: "10px",
                    paddingTop: "0px",
                    fontWeight: "bold",
                    color: "grey"
                  }}
                >
                  {this.props.data.description}
                </p>
                <p
                  style={{
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold",
                    color: "rgb(88, 34, 193)"
                  }}
                >
                  {convertToRupiah(this.calculatePrice(this.props.data))}
                </p>
                <p
                  style={{
                    fontSize: "13px",
                    paddingTop: "0px",
                    fontWeight: "bold",
                    color: "grey"
                  }}
                >
                  {this.functForDataQwt(this.props.data)}
                </p>
              </div>
            )
          }
        </Media>
      </div>
    );
  }

  calculatePrice(data) {
    let total_price = 0;

    if (data.price != null) {
      total_price = data.price;
    } else {
      const { product_variation_orders } = data;
      product_variation_orders.map((variant, i) => {
        return (total_price = variant.price);
      });
    }
    return total_price;
  }

  functForDataQwt(data) {
    const { product_variation_orders } = data;

    let quantity = 0;

    if (data.quantity != null) {
      quantity = data.quantity;
    } else {
      product_variation_orders.map((variant, i) => {
        return (quantity = variant.quantity);
      });
    }

    return data.product
      ? `${quantity} item (${data.product.weight * quantity} gram)`
      : 0;
  }
}

export default PartComponentProduct;
