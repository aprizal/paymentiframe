import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getDataListPayment,
  getDataVaPayment,
  getDataOrder
} from "../Actions/index";
import { withRouter } from "react-router-dom";
import PartComponentProduct from "./PartComponentProduct/partComponentProduct";
import CcDc from "./CcDc/ccdc";
import GoPay from "./GoPay/goPay";
import VaCard from "./VaCard/VaCard";
import ModalForVa from "./ModalForVa/ModalForVa";
import ErrorPage from "./ErrorPage/errorPage";
import ThankYouPage from "./ThankYouPage/thankYouPage";
import UserIdentity from "./UserIdentity/userIdentity";
import ListKurir from "./ListKurir/ListKurir";
import KurirChoose from "./KurirChoose/KurirChoose";
import Modal from "react-modal";
import {
  dataProduk,
  dataPayment,
  dataPaymentMethodReady
} from "../../dataHardCode";
import image from "../image/image";
import { convertToRupiah } from "../Helper";
import "../App.css";

class ChoosePaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: dataProduk,
      dataPayment: dataPayment,
      dataChange: "",
      indicatorPaymentMenu: false,
      dataPaymentMethodReady: dataPaymentMethodReady,
      indicatorPaymentButton: false,
      dataChangePaymentChoose: "",
      loadding: false,
      indicatorModal: false,
      width: 0,
      height: 0,
      amount: 0,
      dataPaymentList: null,
      dataObjectPayment: null,
      authVa: false,
      authEwallet: false,
      authOkBankVa: false,
      dataQuantity: 0,
      totalTagihan: 0,
      heightDisplay: null,
      widthDisplay: null,
      route: null
    };
    this.changeExpedition = this.changeExpedition.bind(this);
    this.paymentChangeUI = this.paymentChangeUI.bind(this);
    this.updateSize = this.updateSize.bind(this);
    this.customStyles = this.customStyles.bind(this);
  }

  render() {
    return this.state.loadding ? (
      <ErrorPage />
    ) : (
      <div className="container" style={{}}>
        <h1
          style={{ fontSize: "18px", paddingTop: "50px", fontWeight: "bold" }}
        >
          Checkout {this.state.data.order_number}
        </h1>
        <div className="row" style={{ marginLeft: "0", marginRight: "0" }}>
          <div
            className="col-sm-7"
            style={{
              paddingRight: "0",
              margin: "0",
              marginBottom: "15px",
              paddingLeft: "0"
            }}
          >
            <UserIdentity data={this.state.data} />
            {/* produklist ================================== >*/}
            <div className="row" style={{ marginLeft: "0", marginRight: "0" }}>
              <div
                className="col-sm-12"
                style={{ padding: "0", paddingRight: "0", margin: "0" }}
              >
                {this.state.data.order_items.map(data => {
                  return <PartComponentProduct data={data} />;
                })}
              </div>
            </div>

            {/*   Total ================================== >*/}
            <div
              style={{
                height: "5px",
                backgroundColor: "lightgrey",
                width: "100%"
              }}
            ></div>
              <div className="row mt-3 justify-content-between mb-4">
              <div className="px-3">
                <p
                  style={{
                    fontSize: "13px",
                    color: "#777",
                    fontWeight: "bold",
                    paddingTop: "0px",
                    marginBottom: "0px"
                  }}
                >
                  Metode Pengiriman
                </p>
              </div>
              <div className="px-3">
                <p
                  style={{
                    fontSize: "13px",
                    fontWeight: "bold",
                    color: "#777",
                    paddingTop: "0px",
                    marginBottom: "0px"
                  }}
                >
                  {this.state.data.shipping_method.toUpperCase()}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <p
                  style={{
                    fontSize: "13px",
                    fontWeight: "bold",
                    paddingTop: "0px",
                    marginBottom: "0px"
                  }}
                >
                  TOTAL
                </p>
              </div>
              <div className="col-6" style={{ textAlign: "end" }}>
                <p
                  style={{
                    fontSize: "13px",
                    fontWeight: "bold",
                    paddingTop: "0px",
                    marginBottom: "0px"
                  }}
                >
                  {convertToRupiah(this.state.data.total_price)}
                </p>
              </div>
            </div>
          </div>
          {/* Ringkasanbelanja ================================== >*/}
          <div className="col-sm-5">
            <div className="stickyDiv">
              <div
                style={{
                  minHeight: "100px",
                  backgroundColor: "white",
                  width: "100%",
                  borderColor: "rgb(88, 34, 193)",
                  borderWidth: "1px",
                  borderStyle: "solid",
                  borderRadius: "8px",
                  textAlign: "-webkit-center"
                }}
              >
                <p
                  className="titleOne"
                  style={{ textAlign: "start", paddingLeft: "5%" }}
                >
                  Ringkasan Belanja
                </p>
                <div
                  style={{
                    height: "1px",
                    backgroundColor: "lightgrey",
                    width: "90%"
                  }}
                ></div>
                <div
                  className="row"
                  style={{
                    paddingLeft: "5%",
                    marginLeft: "0",
                    marginRight: "0"
                  }}
                >
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        marginBottom: "5px"
                      }}
                    >
                      Total Belanja
                    </p>
                  </div>
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        fontWeight: "bold",
                        marginBottom: "5px"
                      }}
                    >
                      {" "}
                      {convertToRupiah(this.state.data.total_price)}
                    </p>
                  </div>
                </div>
                <div
                  style={{
                    height: "1px",
                    backgroundColor: "lightgrey",
                    width: "90%"
                  }}
                ></div>
                <div
                  className="row"
                  style={{
                    paddingLeft: "5%",
                    marginLeft: "0",
                    marginRight: "0"
                  }}
                >
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        marginBottom: "5px"
                      }}
                    >
                      Pengiriman
                    </p>
                  </div>
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        fontWeight: "bold",
                        marginBottom: "5px"
                      }}
                    >
                      {" "}
                      {convertToRupiah(this.state.data.delivery_price)}
                    </p>
                  </div>
                </div>
                <div
                  style={{
                    height: "1px",
                    backgroundColor: "lightgrey",
                    width: "90%"
                  }}
                ></div>
                <div
                  className="row"
                  style={{
                    paddingLeft: "5%",
                    marginLeft: "0",
                    marginRight: "0"
                  }}
                >
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        fontWeight: "bold",
                        marginBottom: "5px"
                      }}
                    >
                      Total Tagihan
                    </p>
                  </div>
                  <div className="col-6">
                    <p
                      style={{
                        fontSize: "13px",
                        textAlign: "start",
                        paddingTop: "5px",
                        marginBottom: "5px"
                      }}
                    >
                      {convertToRupiah(this.state.totalTagihan)}
                    </p>
                  </div>
                </div>
                {/* Paymentlist ================================== >*/}
                <button
                  type="button"
                  class="btn btn-secondary dropdown-toggle"
                  style={{
                    width: "95%",
                    marginTop: "10px",
                    marginBottom: "10px",
                    backgroundColor: "rgb(88, 34, 193)",
                    fontSize: "13px",
                    fontWeight: "bold"
                  }}
                  onClick={() =>
                    this.changeIndicatorPaymentButton(
                      !this.state.indicatorPaymentMenu
                    )
                  }
                >
                  Pilih Metode pembayaran
                </button>

                {this.state.indicatorPaymentButton ? (
                  <div
                    style={{
                      backgroundColor: "#f7f7f7",
                      width: "95%",
                      marginTop: "5px",
                      minHeight: "30vh",
                      borderRadius: "5px",
                      marginBottom: "10px"
                    }}
                  >
                    {this.state.dataPaymentList
                      ? this.state.dataPaymentList.map(data => {
                          return (
                            <div
                              className="row"
                              style={{
                                paddingBottom: "10px",
                                padding: "10px",
                                width: "100%"
                              }}
                              onClick={() => {
                                this.changePayment(data);
                              }}
                            >
                              <div
                                className="col-1"
                                style={{ padding: "0px", alignSelf: "center" }}
                              >
                                <img
                                  src={
                                    this.state.dataObjectPayment
                                      ? this.state.dataObjectPayment
                                          .channel_name == data.channel_name
                                        ? "https://cdn2.iconfinder.com/data/icons/mobile-web-app-vol-5/32/Radio_Button_On_circle_option_active-128.png"
                                        : "https://icons-for-free.com/iconfiles/png/128/radio+button+unchecked+24px-131987943262195581.png"
                                      : "https://icons-for-free.com/iconfiles/png/128/radio+button+unchecked+24px-131987943262195581.png"
                                  }
                                  alt=""
                                  style={{ width: "70%", height: "auto" }}
                                />
                              </div>
                              <div className="col-1" style={{ padding: "0px" }}>
                                <img
                                  src={image(data.channel_code.toLowerCase())}
                                  alt="n"
                                  style={{ width: "90%", height: "auto" }}
                                />
                              </div>
                              <div
                                className="col-10"
                                style={{
                                  alignSelf: "center",
                                  textAlign: "start"
                                }}
                              >
                                <p
                                  style={{
                                    fontSize: "13px",
                                    paddingTop: "0px",
                                    fontWeight: "bold",
                                    color: "grey",
                                    marginBottom: "0"
                                  }}
                                >
                                  {data.channel_name}
                                </p>
                              </div>
                            </div>
                          );
                        })
                      : null}
                  </div>
                ) : null}
                {this.paymentChangeUI()}
              </div>
            </div>
          </div>
        </div>
        {/* Modalsuccesspage ================================== >*/}
        <Modal
          isOpen={this.state.indicatorModal}
          onRequestClose={() => {
            /**this.onClickPayment()**/
          }}
          contentLabel="Example Modal"
          style={this.customStyles()}
        >
          {this.modalType()}
        </Modal>
      </div>
    );
  }

  componentDidMount() {
    if (this.props.route != this.state.route) {
      this.setState({ route: this.props.route }, () => {
        this.props.getDataOrder(this.state.route);
      });
    }
    window.addEventListener("resize", this.updateSize);
  }

  componentWillMount() {
    this.changeExpedition(this.state.dataPayment[0].title);
    this.updateWindowDimensions();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataListPayment != this.props.dataListPayment) {
      this.setState({ dataPaymentList: nextProps.dataListPayment.data });
    }
    if (nextProps.dataOrder != this.props.dataOrder) {
      this.props.getDataListPayment(nextProps.dataOrder.data.vendor_id);
      let count = 0;
      for (
        let index = 0;
        index < nextProps.dataOrder.data.order_items.length;
        index++
      ) {
        let quantity = nextProps.dataOrder.data.order_items[index].item_quantity
          ? nextProps.dataOrder.data.order_items[index].item_quantity
          : 0;
        count += quantity;
      }
      let totalTagihan =
        nextProps.dataOrder.data.total_price +
        parseInt(nextProps.dataOrder.data.delivery_price);
      this.setState({
        data: nextProps.dataOrder.data,
        dataQuantity: count,
        totalTagihan: totalTagihan
      });
    }
    if (nextProps.dataAfterPayEwallet != this.props.dataAfterPayEwallet) {
      if (nextProps.dataAfterPayEwallet.data.checkout_url) {
        window.location.href = nextProps.dataAfterPayEwallet.data.checkout_url;
      }
    }
  }

  updateSize(e) {
    if (e.target.innerWidth > 850) {
      this.setState({ widthDisplay: null });
    } else {
      this.setState({ widthDisplay: e.target.innerWidth });
    }
  }

  customStyles() {
    // if (this.state.widthDisplay == null) {
    if (window.innerWidth > 850) {
      return {
        content: {
          top: "50%",
          left: "50%",
          minHeight: "30vh",
          maxWidth: "50%",
          right: "auto",
          bottom: "auto",
          marginRight: "-50%",
          transform: "translate(-50%, -50%)"
        }
      };
    } else {
      return {
        content: {
          top: "50%",
          left: "50%",
          minHeight: "30vh",
          maxWidth: "90%",
          right: "auto",
          bottom: "auto",
          marginRight: "-50%",
          transform: "translate(-50%, -50%)"
        }
      };
    }
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  changeExpedition(expedition) {
    this.setState({ dataChange: expedition, indicatorPaymentMenu: false });
  }

  changeIndicator(status) {
    this.setState({ indicatorPaymentMenu: status });
  }

  choose() {
    this.props.history.push("/cc");
  }

  changeIndicatorPaymentButton() {
    this.setState({
      indicatorPaymentButton: !this.state.indicatorPaymentButton
    });
  }

  changePayment(data) {
    this.setState(
      {
        dataChangePaymentChoose: data.channel_group,
        indicatorPaymentButton: !this.state.indicatorPaymentButton,
        dataObjectPayment: data
      },
      () => {
        if (this.state.dataChangePaymentChoose == "va") {
          this.setState({ authVa: true, authEwallet: false }, () => {
            this.onClickPayment("openWithAuthVa");
          });
        } else if (this.state.dataChangePaymentChoose == "ewallet") {
          this.setState({ authEwallet: true, authVa: false }, () => {
            this.onClickPayment("openWithAuthEwallet");
          });
        }
      }
    );
  }
  paymentChangeUI() {
    switch (this.state.dataChangePaymentChoose) {
      case "cc":
        return (
          <CcDc
            onClickPayment={() => {
              this.onClickPayment();
            }}
            amount={this.state.totalTagihan}
            data={this.state.data}
          />
        );
        break;
      case "va":
        return null;
        break;
      default:
        null;
        break;
    }
  }

  onClickPayment(value) {
    if (value == "openWithAuthVa") {
      this.setState({ indicatorModal: !this.state.indicatorModal });
    } else if (value == "openWithAuthEwallet") {
      this.setState({ indicatorModal: !this.state.indicatorModal });
    } else {
      this.setState({
        indicatorModal: !this.state.indicatorModal,
        authVa: false,
        authOkBankVa: false,
        authEwallet: false
      });
    }
  }

  onCancelPaymentVa() {
    this.setState({ indicatorModal: !this.state.indicatorModal });
  }

  onClickPaymentVaAuth(value, indicator) {
    switch (indicator) {
      case "cancel":
        this.setState({ indicatorModal: !this.state.indicatorModal });
        break;
      case "ok":
        this.setState({ authOkBankVa: !this.state.authOkBankVa });
        break;
      case "okEwallet":
        this.setState({
          indicatorModal: true,
          authVa: false,
          authOkBankVa: false,
          authEwallet: false
        });
      default:
        break;
    }
  }

  modalType() {
    if (this.state.authOkBankVa) {
      return (
        <VaCard
          dataObjectPayment={this.state.dataObjectPayment}
          onClickPayment={() => this.onClickPayment()}
          totalTagihan={this.state.totalTagihan}
        />
      );
    } else if (this.state.authVa) {
      return (
        <ModalForVa
          onClickPayment={(value, indicator) =>
            this.onClickPaymentVaAuth(value, indicator)
          }
        />
      );
    } else if (this.state.authEwallet) {
      return (
        <ModalForVa
          onClickPayment={(value, indicator) =>
            this.onClickPaymentVaAuth(value, indicator)
          }
          dataObjectPayment={this.state.dataObjectPayment}
          ordernumber={this.state.data}
          data={this.state.data}
        />
      );
    }else {
      return <ThankYouPage onClickPayment={() => this.onClickPayment()} />;
    }
  }
}

const mapState = state => {
  return {
    dataListPayment: state.usersuccessget.dataListPayment,
    dataOrder: state.usersuccessget.dataOrder,
    dataAfterPayEwallet: state.usersuccessget.dataAfterPayEwallet
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDataListPayment: value => dispatch(getDataListPayment(value)),
    getDataVaPayment: (value, id) => dispatch(getDataVaPayment(value, id)),
    getDataOrder: value => dispatch(getDataOrder(value))
  };
};

export default connect(mapState, mapDispatchToProps)(ChoosePaymentMethod);
