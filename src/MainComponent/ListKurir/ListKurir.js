import React, { Component } from 'react';
class ListKurir extends Component {
constructor(props) {
    super(props)
    this.state = {
    }
}

  render() {
    return (
      <div className="row" style={{paddingBottom: '10px', padding: '10px'}} onClick={() => {this.props.changeExpedition(this.props.data.title)}}>
        <div className="col-2">
          <img src={this.props.dataChange == this.props.data.title ? "https://cdn2.iconfinder.com/data/icons/mobile-web-app-vol-5/32/Radio_Button_On_circle_option_active-128.png" : "https://icons-for-free.com/iconfiles/png/128/radio+button+unchecked+24px-131987943262195581.png"} alt="Halosis" style={{width: '100%', height: 'auto'}} />
        </div>
        <div className="col-3">
          <img src={this.props.data.image} alt="Halosis" style={{width: '100%', height: 'auto'}} />
        </div>
        <div className="col-7" style={{alignSelf: 'center'}}>
          <p style={{fontSize: '13px', paddingTop: '0px', fontWeight: 'bold', color: 'grey', marginBottom: '0'}}>{this.props.data.title}</p>
        </div>
      </div>
    )
  }
}

export default ListKurir;