import React, { Component } from 'react';
class KurirChoose extends Component {
constructor(props) {
    super(props)
    this.state = {
    }
}

  render() {
    return (
      <div style={{width: '100%', padding: '0px', margin: '0px', paddingTop: '20px', textAlign: 'center'}}>
        <img src={this.props.data.image} alt="Halosis" style={{width: '100%', height: 'auto'}} />
        <p style={{fontSize: '13px', paddingTop: '20px', fontWeight: 'bold', color: 'blue'}}>Biaya Pengiriman - Rp. {this.props.dataPrice}</p>
      </div>
    )
  }
}

export default KurirChoose;