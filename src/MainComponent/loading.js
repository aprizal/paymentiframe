import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ReactLoading from 'react-loading';
import '../App.css'

class Loading extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    return (
      <div className="containerLoading">
        <ReactLoading type={'spinningBubbles'} style={{fill: 'rgb(88, 34, 193)', height: '5%', width: '5%'}}/>
      </div>
    )
  }
}

export default Loading