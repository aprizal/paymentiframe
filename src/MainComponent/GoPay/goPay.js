import React, { Component } from 'react';
class GoPay extends Component {
constructor(props) {
    super(props)
    this.state = {
      numberCard: '',
    }
}

  render() {
    return (
      <div style={{textAlign: 'start', padding: '10px', textAlign: 'center'}}>
        <img src={"https://66.media.tumblr.com/aa2042add17abd54aabf64a174254468/tumblr_owobx1gX2g1qcrhiyo1_1280.jpg"} alt="Halosis" style={{width: '50%', height: 'auto'}} />
        <p style={{fontSize: '13px',paddingTop: '5px', marginBottom: '5px', paddingTop: '20px'}}>Scan barcode diatas untuk menyelesaikan transaksi, terima kasih.</p>
        
        <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: 'rgb(88, 34, 193)'}}>
          <p style={{fontSize: '13px', fontWeight: 'bold', margin: '5px'}} onClick={() => {this.handleClickPay()}}>Sudah Terbayar</p>
        </button>

      </div>
    )
  }

  handleClickPay() {
    this.props.onClickPayment()
  }
}

export default GoPay;