import React, { Component } from 'react';
import { connect } from 'react-redux'
import { withRouter, Router } from "react-router-dom";
import image from '../../image/image'
import { getDataVaPayment } from '../../Actions/index'
import Swal from 'sweetalert2'
class VaCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null,
      dataVaFromApi: null
    }
  }

  render() {
    return (
      <div>
        <p style={{fontSize: '18px', fontWeight: 'bold', paddingTop: '10px', textAlign: 'center'}}>BANK Virtual Account</p>
        <img src={image(this.props.dataObjectPayment.channel_code.toLowerCase())} alt="Halosis" style={{width: '100%', height: 'auto'}} />
        <p style={{fontSize: '13px', fontWeight: 'bold', paddingTop: '10px', textAlign: 'start', paddingLeft: '5%'}}>Detail Pembayaran: </p>
        <p style={{fontSize: '13px', fontWeight: 'bold', paddingTop: '15px', textAlign: 'start', paddingLeft: '5%', marginBottom: '0'}}>Nomor VA: </p>
        <div className="row" style={{borderColor: '#f7f7f7', borderWidth: '1px', borderStyle: 'solid', margin: '3%', marginTop: '0', padding: '0', backgroundColor: '#f7f7f7', borderRadius: '25px', paddingTop: '5px', paddingBottom: '5px'}}>
          <div className="col-8" style={{padding: '0'}}>
            <p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}>{this.state.dataVaFromApi ? this.sliceDataVA(this.state.dataVaFromApi.account_number) : "000 000 000 000"}</p>
          </div>
          <div className="col-4">
            <a href=""><p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}>Salin No</p></a>
          </div>
        </div>

        <p style={{fontSize: '13px', fontWeight: 'bold', paddingTop: '0px', textAlign: 'start', paddingLeft: '5%', marginBottom: '0'}}>Metode pembayaran Akan Berakhir Pada: </p>
        <div className="row" style={{borderColor: '#f7f7f7', borderWidth: '1px', borderStyle: 'solid', margin: '3%', marginTop: '0', padding: '0', backgroundColor: '#f7f7f7', borderRadius: '25px', paddingTop: '5px', paddingBottom: '5px'}}>
          <div className="col-8" style={{padding: '0'}}>
            <p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}>{this.state.dataVaFromApi ? this.state.dataVaFromApi.expiration_date : null}</p>
          </div>
          <div className="col-4">
            <a href=""><p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}></p></a>
          </div>
        </div>

        <p style={{fontSize: '13px', fontWeight: 'bold', paddingTop: '0px', textAlign: 'start', paddingLeft: '5%', marginBottom: '0'}}>Jumlah Yang Harus Dibayarkan: </p>
        <div className="row" style={{borderColor: '#f7f7f7', borderWidth: '1px', borderStyle: 'solid', margin: '3%', marginTop: '0', padding: '0', backgroundColor: '#f7f7f7', borderRadius: '25px', paddingTop: '5px', paddingBottom: '5px'}}>
          <div className="col-8" style={{padding: '0'}}>
            <p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}>Rp. {this.props.totalTagihan}</p>
          </div>
          <div className="col-4">
            <a href=""><p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '3%',}}></p></a>
          </div>
        </div>


        <p style={{fontSize: '13px', marginBottom: '0',textAlign: 'start', paddingLeft: '5%', paddingBottom: '10px', color: 'rgb(88, 34, 193)'}}>Jumlah transver harus sama dengan jumlah yang harus dibayarkan untuk memastikan sistem dapat mengenali akun anda</p>

        <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: 'rgb(88, 34, 193)'}}><p className="textButton" onClick={() => {this.handleClick()}}>Kembali ke Dasboard</p></button>
      </div>
    )
  }

  componentDidMount() {
    this.setState({data: this.props.dataObjectPayment}, () => {
      this.props.getDataVaPayment(this.props.dataObjectPayment, location.pathname.substr(1))
    })
  }
  
  componentWillReceiveProps(nextProps) {
    // console.log('data VA ==> ', nextProps.dataVa)
    if (nextProps.dataObjectPayment != this.state.data) {
      this.setState({data: nextProps.dataObjectPayment}, () => {
        this.props.getDataVaPayment(nextProps.dataObjectPayment, location.pathname.substr(1))
      })
    }
    if (nextProps.dataVa != this.props.dataVa) {
      if (nextProps.dataVa.data.code != 'BANK_NOT_SUPPORTED_ERROR') {
        this.setState({dataVaFromApi: nextProps.dataVa.data})
      } else {
        this.props.onClickPayment()
        Swal.fire('Terdapat kesalahan pada pembuatan akun VA Bank ini, silahkan memilih metode pembayaran lainnya')
      }
    }
  }

  sliceDataVA(data) {
    let arr = data.split('')
    let newArr = []
    let count = 0
    for (let index = 0; index < arr.length; index++) {
      if (count < 4) {
        count+=1
        newArr.push(arr[index])
      } else {
        count=0
        count+=1
        newArr.push(' ')
        newArr.push(arr[index])
      }
    }
    return newArr.join('')
  }

  handleClick() {
    this.props.onClickPayment()
  }
}

const mapState = state => {
  return {
    dataVa: state.usersuccessget.dataVa
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDataVaPayment: (value, id) => dispatch(getDataVaPayment(value, id)),
  }
}

export default connect(mapState, mapDispatchToProps)(VaCard);