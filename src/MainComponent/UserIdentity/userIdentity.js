import React, { Component } from 'react';
class UserIdentity extends Component {
  render() {
    return (
      <div>
        <p className="titleOne">Alamat Pengiriman</p>
        <div style={{height: '1px', backgroundColor: 'grey', width: '100%'}}></div>
        <p className="titleOne" style={{marginBottom: '0px'}}>{this.props.data.delivery_name}</p>
        <p style={{fontSize: '13px', paddingTop: '0px'}}>{this.props.data.delivery_phone}</p>
        <p style={{fontSize: '13px', paddingTop: '0px', marginBottom: '0'}}>{this.props.data.delivery_address+' '+this.props.data.delivery_area+' '+this.props.data.delivery_city_id}</p>
        <p style={{color: 'grey', marginTop: '0', paddingTop: '0', fontSize: '10px', paddingTop: '0px', fontWeight: 'bold', }}>{this.props.data.address_note != "null" ? this.props.data.address_note : ""}</p>
        <div style={{height: '5px', backgroundColor: 'lightgrey', width: '100%'}}></div>
      </div>
    )
  }
}

export default UserIdentity;