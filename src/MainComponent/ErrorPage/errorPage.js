import React, { Component } from 'react';
class ErrorPage extends Component {
  render() {
    return (
      <div style={{height: '100vh', backgroundColor: '#f7f7f7', width: '100%', top: '0', position: 'sticky', textAlign: 'center'}}>
        <img src={"https://www.kursuswebsite.org/wp-content/uploads/2016/11/58f19d95.loading.gif"} alt="Halosis" style={{paddingTop: '37vh',width: '10%', height: 'auto'}} />
      </div>
    )
  }
}

export default ErrorPage;