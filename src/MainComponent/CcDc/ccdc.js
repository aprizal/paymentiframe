import React, { Component } from 'react';
import { connect } from 'react-redux'
import Xendit from 'xendit-js-node';
import Iframe from 'react-iframe'
import './ccdc.css'
import Modal from 'react-modal';
import { chargeCC } from '../../Actions/index'
import Cleave from 'cleave.js/react'
import Swal from 'sweetalert2'
class CcDc extends Component {
constructor(props) {
    super(props)
    this.state = {
      numberCard: '',
      month: '',
      year: '',
      cvv: '',
      linkCCTokenize: null,
      indicatorModal: false,
      widthDisplay: null,
      data: null
    }
    this.onChangeText = this.onChangeText.bind(this);
    this._tokenResponseHandler = this._tokenResponseHandler.bind(this);
    this._handleIframeTask = this._handleIframeTask.bind(this);
    this.updateSize = this.updateSize.bind(this)
}

  render() {
    return (
      <div className="containerCCDC">
        <p className="textNomorKartu">Nomor Kartu</p>
        <Cleave placeholder="Enter your credit card number"
          class="form-control reDevineInputForMonthYear"
          options={{creditCard: true}}
          placeholder="e.g 12** **** **** **54"
          onChange={(e) => this.onChangeText(e, 'handleChangeNumberCard')} />

        {/* <input type="tel" class="form-control reDevineInputForMonthYear" placeholder="e.g 12** **** **** **54" value={this.state.numberCard} onChange={(e) => this.onChangeText(e, 'handleChangeNumberCard')}/> */}
        <div className="row containerRowForMonthAndYearCvv">

          <div className="col-6 containerColSixMonthYearCvv">
            <p className="textValidPeriod">Masa Berlaku</p>
            <div className="row changeRowCssMonthYearCvv">
              <div className="col-5 reDevineColFiveForCCDC">
                <input type="text" class="form-control reDevineInputForMonthYear" maxlength="2" placeholder="e.g 01" value={this.state.month} onChange={(e) => {this.onChangeText(e, 'handleChangeMonthCard')}}/>
              </div>
              <div className="col-1 reDevineColOneForCCDC">
                <h3 className="forGarisMiringCcDc">/</h3>
              </div>
              <div className="col-6 reDevineColSixForCCDC">
                <input type="text" class="form-control reDevineInputForMonthYear" maxlength="4" placeholder="e.g 2020" value={this.state.year} onChange={(e) => this.onChangeText(e, 'handleChangeYearCard')}/>
              </div>
            </div>
          </div>

          <div className="col-6 containerColSixMonthYearCvv">
            <p className="textValidPeriod">CVV</p>
            <div className="row changeRowCssMonthYearCvv">
              <div className="col-8 reDevineColEightForCcDc">
                <input type="text" class="form-control reDevineInputForMonthYear" maxlength="4" placeholder="e.g ***" value={this.state.cvv} onChange={(e) => this.onChangeText(e, 'handleChangeCVVCard')}/>
              </div>
              <div className="col-4 reDevineColFourForCcDc">
                <img src={"https://image.flaticon.com/icons/svg/62/62936.svg"} alt="Halosis" className="imageSizeCvv"/>
              </div>
            </div>
          </div>
        </div>

        <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: this.authenticationButton()}}>
          <p className="textForButtonCcDc" onClick={() => {this.tokenize()}}>Pembayaran</p>
        </button>

        
        <Modal
          isOpen={this.state.indicatorModal}
          onRequestClose={() => { /**this.onClickPayment()**/ }}
          contentLabel="Example Modal"
          style={this.customStyles()}
        >
          {
            this.state.linkCCTokenize ?
            <Iframe url={this.state.linkCCTokenize}
            width="500px"
            height="380px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" allowFullScreen/>:null
          }
        </Modal>
      </div>
    )
  }

  componentDidMount() {
    window.addEventListener('message', this._handleIframeTask);
    window.addEventListener('resize',this.updateSize);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataChargeCC != this.props.dataChargeCC) {
      if (nextProps.dataChargeCC.data.status == 'FAILED') {
        // this.setState({linkCCTokenize: null, indicatorModal: false}, () => {
        //   this.props.onClickPayment()
        // })
        this.setState({linkCCTokenize: null, indicatorModal: false}, () => {
          Swal.fire('Pembayaran anda gagal, cek kembali kartu kredit yang anda gunakan')
        })
        
      } else {
        this.setState({linkCCTokenize: null, indicatorModal: false}, () => {
          this.props.onClickPayment()
        })
      }
    }
  }

  _handleIframeTask (e) {
    let dataToke = JSON.parse(e.data)
    if (dataToke.status == 'VERIFIED') {
    //   Xendit.setPublishableKey('xnd_public_production_VDjpblZBOURr4VRPxMm2GrAwyLBXJmQIox7zOhG2jGJtP6v3WzUCwQiqXp5l7a');
    //   Xendit.card.createAuthentication({
    //     amount: '100000',
    //     token_id: this.state.data.authentication_id
    //   }, function (err, data) {
    //     if (err) {
    //       console.log(err)
    //     }
    
    //     if (data.status === 'VERIFIED') {
    //       console.log(data)
    //         // Penanganan keberhasilan
    //     } else if (data.status === 'IN_REVIEW') {
    //       console.log(data)
    //         // Penanganan otentikasi (3DS)
    //     } else if (data.status === 'FAILED') {
    //       console.log(data)
    //         // Penanganan kegagalan
    //     }
    // })


      this.props.chargeCC(this.state.data, this.props.amount, this.props.data.order_number)
     
    }
    if (dataToke.status == 'FAILED') {
      this.setState({linkCCTokenize: null, indicatorModal: false})
    }
    // if (e.origin !== 'https://localhost:9000/') {
    //   console.log(typeof dataToke)
    //   if (dataToke.status == 'VERIFIED') {
    //     this.setState({linkCCTokenize: null}, () => {
    //       console.log(typeof dataToke)
    //     })
    //   }
    // } else {
    //   if (dataToke.status == 'VERIFIED') {
    //     this.setState({linkCCTokenize: null}, () => {
    //       console.log(typeof dataToke)
    //     })
    //   }
    // }
  };
  
  onChangeText(e, indicator) {
    // if (Number(e.target.value) || !e.target.value) {
      var patt1 = /[0-9]/g;
      var result = e.target.value.match(patt1);
      if (result || !e.target.value) {
        switch (indicator) {
          case 'handleChangeNumberCard':
            this.setState({numberCard: e.target.value}, () => {
            })
            break;
          case 'handleChangeMonthCard':
            this.setState({month: e.target.value}, () => {
            })
            break;
          case 'handleChangeYearCard':
            this.setState({year: e.target.value}, () => {
            })
            break;
          case 'handleChangeCVVCard':
            this.setState({cvv: e.target.value}, () => {
            })
            break;
          default:
            break;
        }
        
      }
    // }
  }

  authenticationButton() {
    if (this.state.numberCard && this.state.month && this.state.year && this.state.cvv) {
      return 'rgb(88, 34, 193)'
    } else {
      return 'grey'
    }
  }

  handleClickPay() {
    if (this.state.numberCard && this.state.month && this.state.year && this.state.cvv) {
      this.props.onClickPayment()
    } else {
    }
  }


  tokenize() {
    let newNumberCard = this.state.numberCard.split(' ')
    let tokenData = {
      "amount": this.props.amount,        
      "card_number": newNumberCard.join(''),        
      "card_exp_month": this.checkMounth(this.state.month),        
      "card_exp_year": this.state.year,        
      "card_cvn": this.state.cvv,
      "is_multiple_use": false,
      "should_authenticate": true,
    }
    Xendit.setPublishableKey('xnd_public_production_VDjpblZBOURr4VRPxMm2GrAwyLBXJmQIox7zOhG2jGJtP6v3WzUCwQiqXp5l7a');
    Xendit.card.createToken(tokenData, this._tokenResponseHandler);
  }

  checkMounth(data) {
    if (data.length == 1) {
      return '0'+data
    } else {
      return data
    }
  }

  _tokenResponseHandler(err, token) {
    if (err) {
      console.log(err);
      this.setIsTokenizing();

      return;
    }
    
    switch (token.status) {
      case 'APPROVED':
        console.log('approve');
      case 'VERIFIED':
        console.log('verified');
      case 'FAILED':
        console.log('failed');
        break;
      case 'IN_REVIEW':
        this.setState({
          linkCCTokenize: token.payer_authentication_url, data: token, indicatorModal: !this.state.indicatorModal
        }, () => {
          console.log('inreview');
        });

        break;
      default:
        alert('Unknown token status');
        break;
    }
  }

  updateSize(e) {
    if (e.target.innerWidth > 850) {
      this.setState({widthDisplay: null})
    } else {
      this.setState({widthDisplay: e.target.innerWidth})
    }
  }

  customStyles() {
    if (this.state.widthDisplay == null) {
      return {
        content : {
          top                   : '50%',
          left                  : '50%',
          minHeight             : '30vh',
          maxWidth              : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
        }
      }
    } else {
      return {
        content : {
          top                   : '50%',
          left                  : '50%',
          minHeight             : '30vh',
          maxWidth              : '90%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
        }
      }
    }
  }
}

// export default CcDc;
const mapState = state => {
  return {
    dataChargeCC: state.usersuccessget.dataChargeCC,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    chargeCC: (value, amount, id) => dispatch(chargeCC(value, amount, id)),
  }
}

export default connect(mapState, mapDispatchToProps)(CcDc);