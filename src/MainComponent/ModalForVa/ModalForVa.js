import React, { Component } from 'react';
import { connect } from 'react-redux'
import { getPayOrderEwallet } from '../../Actions/index'
class ModalForVa extends Component {

  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <p className="titlePaymentSuccess">Anda akan melakukan pembayaran {this.props.dataObjectPayment ? this.props.dataObjectPayment.channel_code : "Via Virtual Account"}</p>

        <img src={this.imageFromLogoPayment(this.props.dataObjectPayment ? this.props.dataObjectPayment : "")} alt="Halosis" style={{width: '30%', height: 'auto', padding: '10px'}} />
 
        <div className="row" style={{padding: '0', margin: '0'}} onClick={() => {this.props.dataObjectPayment ? this.handleClick(null, 'ewallet') : this.handleClick(null, 'ok')}}>
          <div className="col-6" style={{padding: '5px', margin: '0'}}>
            <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: 'rgb(88, 34, 193)'}}><p className="textButton">Bayar</p></button>
          </div>
          <div className="col-6" style={{padding: '5px', margin: '0'}} onClick={() => {this.handleClick(null, 'cancel')}}>
            <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: 'red'}}><p className="textButton">Cancel</p></button>
          </div>
        </div>
      </div>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataAfterPayEwallet != this.props.dataAfterPayEwallet) {
      if (nextProps.dataAfterPayEwallet.data.ewallet_type) {
        // console.log('data after pay ewallet => ', nextProps.dataAfterPayEwallet)
        this.props.onClickPayment(null, 'okEwallet')
      } else {
        console.log('')
      }
    }
  }

  imageFromLogoPayment(data) {
    switch (data.channel_code) {
      case 'OVO':
        return "https://apprecs.org/gp/images/app-icons/300/4f/ovo.id.jpg"
        break;  
      case 'DANA':
        return "https://cdn.techinasia.com/data/images/8e80279bec7a5f6d0b7097d107426abf.png"
        break
      default:
        return "https://5garuda.com/media/image/153207297320215b51940b8f5e8.jpg"
        break;
    }
  }

  handleClick(value, indicator) {
    switch (indicator) {
      case 'cancel':
        this.props.onClickPayment(value, indicator)
        break;
      case 'ok':
        this.props.onClickPayment(value, indicator)
        break;
      case 'ewallet':
        this.props.getPayOrderEwallet(this.props.ordernumber, this.props.dataObjectPayment, this.props.data)
        // this.props.onClickPayment(value, indicator)
        break;
      default:
        break;
    }
  }
}

const mapState = state => {
  return {
    dataAfterPayEwallet: state.usersuccessget.dataAfterPayEwallet,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPayOrderEwallet: (value, valuePayment, dataProduk) => dispatch(getPayOrderEwallet(value, valuePayment, dataProduk)),
  }
}

export default connect(mapState, mapDispatchToProps)(ModalForVa);