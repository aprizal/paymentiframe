import React, { Component } from 'react';
class ThankYouPage extends Component {
  componentWillMount(){
    console.log('thi:', this.props)
  }
  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <p className="titlePaymentSuccess">Pembayaran Anda Berhasil </p>

        <img src={"http://icons.iconarchive.com/icons/streamlineicons/streamline-ux-free/1024/monitor-cash-credit-card-icon.png"} alt="Halosis" style={{width: '50%', height: 'auto'}} />
        
        <p className="contentPaymentSuccess">Pengiriman barang akan segera kami proses</p>

        <button type="button" class="btn btn-primary btn-lg btn-block" style={{padding: '0px', margin: '0px', marginTop: '10px', backgroundColor: 'rgb(88, 34, 193)'}}><p className="textButton" onClick={() => {this.handleClick()}}>Selesai</p></button>
      </div>

    )
  }

  handleClick() {
    this.props.onClickPayment()
  }
}

export default ThankYouPage;