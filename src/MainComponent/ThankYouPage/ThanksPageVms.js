import React, { Component } from 'react';

class ThanksPageVms extends Component {
  render() {
    return (
      <div class="success-page">
        <h2 class="h2-title">Pembayaran Anda Berhasil</h2>
        <img src={"http://icons.iconarchive.com/icons/streamlineicons/streamline-ux-free/1024/monitor-cash-credit-card-icon.png"} alt="Halosis" style={{ width: '80%', height: 'auto' }} />
        <p>Pengiriman barang akan segera kami proses</p>
        <br/>
        <a href="https://vms.halosis.co.id">Login Sekarang</a>
      </div>
    )
  }

  //   handleClick() {
  //     this.props.onClickPayment()
  //   }
}

export default ThanksPageVms;