const dataProduk = {
  "id": 1,
  "vendor_id": 1,
  "customer_id": 1,
  "dropshipper_id": null,
  "dropshipper_name": null,
  "dropshipper_phone": null,
  "total_price": 0,
  "delivery_price": 0,
  "status": "NEW",
  "tracking_code": null,
  "delivery_name": "Tidak ada data pembeli",
  "delivery_address": "Alamat tidak tersedia",
  "delivery_area": "",
  "address_note": "Note tidak tersedia",
  "delivery_city_id": "-",
  "delivery_weight": 1,
  "delivery_phone": "-",
  "payment_method": null,
  "shipping_method": "JNE",
  "order_number": "000 000 000",
  "printed_label": null,
  "printed_invoice": null,
  "customer_bank_name": null,
  "payment_image": null,
  "receipt_sender_name": null,
  "receipt_uploaded_at": null,
  "delivered_at": null,
  "paid_at": null,
  "shipped_at": null,
  "created_at": "2020-01-27T09:32:07.000Z",
  "updated_at": "2020-12-27T09:32:07.000Z",
  "delivery_address_value": "493|42|6",
  "customer_name": "Juheri Daud",
  "customer_phone": "628123456789",
  "promotion_id": null,
  "promotion_type": null,
  "value": null,
  "order_items": [
      {
          "order_item_id": 1,
          "product_id": 1,
          "quantity": 5,
          "price": 1,
          "product_name": "No Product",
          "description": "No Data Description",
          "product_code": "ZFE-9999",
          "product_status": "ACTIVE",
          "product_image": "images_1578459004057.jpg",
          "variant_type": null,
          "variant_name": null,
          "weight": '0'
      }
  ]
}

const dataPayment = [
  {
    image: "https://pngimage.net/wp-content/uploads/2018/06/jne-png-7.png",
    title: 'JNE',
    harga: '0'
  },
  {
    image: "https://www.youngontop.com/wp-content/uploads/2019/05/logo-tiki.png",
    title: 'Tiki',
    harga: '0'
  },
  {
    image: "https://www.nuwori.com/wp-content/uploads/2019/02/logo-jnt-png-merah.png",
    title: 'JNT',
    harga: '0'
  },
  {
    image: "https://i2.wp.com/seismicell.com/wp-content/uploads/2018/05/logo.png?resize=1000%2C426&ssl=1",
    title: 'Ninja Express',
    harga: '0'
  },
  {
    image: "https://amrnet18.files.wordpress.com/2017/10/go-send-by-go-jek-logo.png",
    title: 'Gosend',
    harga: '0'
  }
]

const dataPaymentMethodReady = [
  {
    image: "http://simpleicon.com/wp-content/uploads/credit-card-7-256x256.png",
    title: "Creadit Card / Debet Card"
  },
  {
    image: "https://res-4.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/uvlqkxhwj2lblqapmlql",
    title: "GoPay"
  },
  {
    image: "https://upload.wikimedia.org/wikipedia/id/thumb/e/e0/BCA_logo.svg/472px-BCA_logo.svg.png",
    title: "Virtual Account BCA"
  },
  {
    image: "https://jobsmicro.com/wp-content/uploads/job-manager-uploads/company_logo/2019/01/Bank-Mandiri-Persero-Tbk..png",
    title: "Virtual Account Mandiri"
  },
  {
    image: "https://upload.wikimedia.org/wikipedia/en/thumb/2/27/BankNegaraIndonesia46-logo.svg/1200px-BankNegaraIndonesia46-logo.svg.png",
    title: "Virtual Account BNI"
  }
]
module.exports = {
  dataProduk,
  dataPayment,
  dataPaymentMethodReady
}